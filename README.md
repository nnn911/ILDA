# Interfacial Line Defect Analysis (ILDA)

## Description

ILDA is a Python-based tool for identifying and extracting interfacial line defects in atomistic datasets. It runs in the visualization and analysis program [OVITO Pro](https://www.ovito.org/about/ovito-pro/). Unlike OVITO Basic which is free for use, OVITO Pro requires a paid subscription. ILDA uses the Python script capability in OVITO Pro while also leveraging numerous built-in modifiers in OVITO (neighbor finders, surface mesh construction).

ILDA is designed to have similar functionality to the well-established dislocation extraction algorithm (DXA), which is also [available in OVITO](https://www.ovito.org/docs/current/reference/pipelines/modifiers/dislocation_analysis.html). Given an atomistic dataset containing a semi-coherent interface, ILDA will identify all interfacial dislocations and disconnections, and insert a line segment representation of the defects. Each line segment has a Burgers vector and step height associated with it. These line segments are currently represented as bond objects in OVITO. ILDA can be utilized in conjunction with DXA to provide a comprehensive analysis of line defects in an atomistic system.

## Tutorial
First time users may find [this tutorial](https://www.youtube.com/watch?v=O0tElGuW7pA) useful, which demonstrates how to use ILDA from start-to-finish.

## Usage
Currently ILDA has only been tested as a [Python script modifier in the OVITO GUI](https://www.ovito.org/docs/current/reference/pipelines/modifiers/python_script.html). In the near future, we will extend ILDA so that it can also be used when calling OVITO via command-line with the `ovitos` command.

ILDA is currently designed to operate on a single interface at a time, which is defined as the inteface between _Crystal A_ and _Crystal B_. The user must select what pair of crystals to consider (see below).

ILDA has only been tested to date with cubic crystals (FCC and BCC). If you wish to apply ILDA to a different system, please notify us (<ryan.sills@rutgers.edu>). In principle, the `estimateF` option should work for any crystal structure which the polyhdral template matching modifier is able to identify. In the case of FCC crystals, ILDA can handle the presence of stacking faults (i.e., from extended dislocations). 

ILDA has only been tested on interfaces with periodic boundary conditions. Non-periodic interfaces may cause issues.
 
When using ILDA, perform the following steps after opening an atomistic dataset of interest in OVITO:

1. Perform phase identifaction and grain segmentation using the [Polyhedral template matching](https://www.ovito.org/docs/current/reference/pipelines/modifiers/polyhedral_template_matching.html) and [Grain segmentation](https://www.ovito.org/docs/current/reference/pipelines/modifiers/grain_segmentation.html) modifiers.
2. Add a [Python script modifier](https://www.ovito.org/docs/current/reference/pipelines/modifiers/python_script.html) to the pipeline. Load the the scripts/gui.py file.
3. In the text editor, modify the `sys.path.append()` command to specify the path to the ILDA src folder on your machine as follows (in the future, we hope to elminate the need for this step):
```
sys.path.append("YOUR_PATH/src/")
```
4. Select an atom from each crystal to serve as the "fiducial atom" for that crystal. Enter the associated Particle Identifers in the `atomA` and `atomB` fields. Crystal A contains `atomA` and Crystal B contains `atomB`.
5. By default, ILDA attempts to identify co-incidence site (CIS) atoms in the interface. If successfully identified, these atoms will be colored yellow. If ILDA fails to find any CIS atoms, the user can attempt the following:
    - Adjust the `RMS cut-off` used by Polyhedral template matching. If the RMS cut-off is too small, CIS atoms may be missed. If it is too large, spurious CIS atoms may be identified, which will lead to spurious line defects.
    - Increase the `cis_tol` parameter above zero. If this parameter is too large, spurious CIS atoms may be identified (this feature is still under development).
     
If ILDA is still unable to find CIS atoms, it is not possible to proceed forward. Please let us know (<ryan.sills@rutgers.edu>) if you encounter this situtation, so that we can continue to expand ILDA's utility.

Assuming that ILDA successfully identified CIS atoms, there are two options for proceeding with line defect extraction. These options differ in their coherent reference frames used to define the Burgers vectors. Using the `estimateF` option, the coherent reference frame is automatically estimated by ILDA. However since this is an estimate, the Burgers vectors reported by ILDA are estimated and some Burgers vectors will be spurious (e.g., nonzero Burgers vector in the interface where no line defect is present). The user must exercise caution when using this option to ensure that the output is properly interpreted. 

Alternatively, when the `estimateF` option is not used, the user must manually specify the coherent reference frame. The `xA` and `yA` vectors specify the orientation of Crystal A in the coherent reference frame, and the `xB` and `yB` vectors specify the orientation of Crystal B. The `EcohA` and `EcohB` matrices specify the coherency strains on the two crystals in the coherent reference frame. Note that the specified crystal orientations _must be close to the crystal orientations in the simulation cell_ (i.e., the crystallographic direction of Crystal A oriented along the x axis of the simulation cell must be close to the `xA` direction.)

In either case, the coherent reference frame is specified by the deformation gradients _FA_ and _FB_ which map from the crystal reference frames to the coherent reference frame. These matrices are printed when ILDA runs.

6. To continue to the line extraction process, enable the `extract_lines` option. ILDA will then automatically proceed with steps to construct a surface mesh of the interface, construct Burgers circuits, and then insert bond objects to form the defect lines. The following parameters may be controlled the change the behavior:
    - `aA` and `aB` are the lattice constants of Crystals A and B
    - `Rsphere` is the radius of the sphere used to construct the surface mesh with the [alpha-shape method](https://www.ovito.org/docs/current/reference/pipelines/modifiers/construct_surface_mesh.html) that is built into OVITO. 
    - `btol` is the Burgers vector magnitude used to distinguish distinct Burgers vector IDs from each other (i.e., magnitudes must differ by more than `btol` to be assigned distinct IDs), and if a Burgers vector has magnitude less than `btol` it is ignored.
    - `angtol` is the angular tolerance in degrees used to distinguish distinct Burgers vector variants for each ID (i.e., two Burgers vectors with the same ID must differ in their direction by more than `angtol` to be assigned distinct variants). 
    - `distF` defines the skin distance from the interface used to estimate the coherent reference frame. If `distF` is too small or too large, the estimated coherent reference frame may be low quality leading to inaccurate Burgers vectors.

After running, all line defect information is stored in `Bonds` which connect _Node_ atoms (colored black). Each bond has a `Burgers Vector` and `Step Height` property describing the line defect. ILDA tries to organize the resulting line defects into Burgers IDs (each ID has a distinct Burgers vector magnitude), and then within each ID a set of variants (each variant has a distinct Burgers vector direction). This information is printed in the Python modifier window. Each Burgers ID has a unique color assigned to it. Finally, the user may also visualize the `Surface mesh` used to perform the analysis (default transparency is 100%, decrease to visualize it).




## Support
If you encounter issues using ILDA, please contact Ryan Sills at <ryan.sills@rutgers.edu>.

## Authors and acknowledgment
ILDA was developed by Dr. Nipal Deka and Prof. Ryan Sills of the Department of Materials Science and Engineering at Rutgers University, in collaboration with Dr. Alexander Stukowski of OVITO GmbH. Its development was supported by the U.S. Department of Energy, Office of Science, Basic Energy Sciences, under Award # DE-SC0022154 (N.D. and R.B.S.). The ILDA algorithm will be documented in a forthcoming journal publication.

## License
ILDA is open-source software, see the LICENSE file for more details.
