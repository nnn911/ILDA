#Some utility functions
import ovito
from ovito.data import PTMNeighborFinder

import numpy as np
import math

def dotproduct(v1, v2):
    return sum((a*b) for a, b in zip(v1, v2))

def length(v):
    return math.sqrt(dotproduct(v, v))

def angle(v1, v2):
    return math.acos(max(min(dotproduct(v1, v2) / (length(v1) * length(v2)),1),-1))

def unit(v1):
    v1 /= np.linalg.norm(v1)
    return v1

def parallel_test(v1,v2,angtol):    
    # test if two vectors are parallel witin angle angtol
    ang = abs(angle(v1,v2))
    sign = 1
    if ang>math.pi/2:
        sign = -1
        ang = math.pi-ang
    return ang<angtol,sign

def pbc_vec(h_matrix,vec):
    # get the correct image of a given vector which stays inside the simulation cell
    vecP= np.zeros(3)
    vecPs= np.zeros(3)
    #scaled cordinates
    vecR= np.matmul(np.linalg.inv(h_matrix), vec)
    
    #PBC
    vecPs[0]= vecR[0]- np.rint( vecR[0])
    vecPs[1]= vecR[1]- np.rint( vecR[1])
    vecPs[2]= vecR[2]- np.rint( vecR[2])
    
    #real cordinates
    vecP= np.matmul(h_matrix, vecPs)
   
# cubic cell   
    #vecP[0]= vec[0]- np.rint( vec[0]/ Lx)* Lx
    #vecP[1]= vec[1]- np.rint( vec[1]/ Ly)* Ly
    #vecP[2]= vec[2]- np.rint( vec[2]/ Lz)* Lz
        
    return vecP
    
def scalePTMVec(type,a):
    #the scaling here is due to the convention in PTM that polyhedral template vectors are scaled to have average length of 1
    if type== 1:  #FCC
        a=a/math.sqrt(2)
    elif type== 2: #HCP
        a=a           
    elif type== 3: #BCC
        a=a/(14*math.sqrt(3)/3-7) #1.0829
    return a
    
def scalePTMInteratomicDistance(type,a):
    #the scaling here is due to the meaning of the "Interatomic Distance" parameter in PTM
    if type== 1:  #FCC
        a=a*math.sqrt(2)
    elif type== 2: #HCP
        a=a*math.sqrt(2)         
    elif type== 3: #BCC
        a=a*math.sqrt(4/3)
    return a
    
def totalDeformationGradPTM(self, pindex, type, grainid ): 
    """Total deformation gradient of an atom from LS minimization of all neighbor atoms"""
    
    ptm_query = PTMNeighborFinder.Query(self.neigh_finder) 
        
    if grainid==self.g1_id:
        ref_orientation=self.g1_orientation 
        a=self.aA    
    elif grainid==self.g2_id:
        ref_orientation=self.g2_orientation
        a=self.aB             
    ptm_query.find_neighbors(pindex, ref_orientation)          
    
    a = scalePTMVec(type,a)
   
    V=np.zeros((3,3)) 
    W=np.zeros((3,3)) 
            
    for j in range(ptm_query.count):
        neigh = ptm_query[j]            
        W+=np.outer(pbc_vec(self.h_matrix,neigh.delta),np.array(neigh.ideal_vector)*a)    
        V+=np.outer(np.array(neigh.ideal_vector)*a,np.array(neigh.ideal_vector)*a)
                   
    Ft=np.matmul(W,np.linalg.inv(V))
      
    return Ft
    #return np.linalg.inv(Ft)
    
def decompose_F(F):
    tol = 1e-1
    F = np.array(F)
    tmp = np.matmul(F,np.transpose(F))
    #print(tmp)
    #make sure there isn't a shear deformation
    # if (abs(tmp[0,1])>tol or abs(tmp[0,2])>tol or
    #     abs(tmp[1,0])>tol or abs(tmp[1,2])>tol or
    #     abs(tmp[2,0])>tol or abs(tmp[2,1])>tol):
    #    raise RuntimeError('Specified F has a shear deformation - not allowed!')
       
    sx = math.sqrt(tmp[0,0])
    sy = math.sqrt(tmp[1,1])
    sz = math.sqrt(tmp[2,2])
    R = np.vstack((F[0,:]/sx,F[1,:]/sy,F[2,:]/sz))
    #print(R)
    return R
    
    
def RotMat_to_Quat(R):
    # convert rotation matrix to quaternion
    qw = math.sqrt(1+R[0,0]+R[1,1]+R[2,2])/2
    qx = (R[2,1]-R[1,2])/(4*qw)
    qy = (R[0,2]-R[2,0])/(4*qw)
    qz = (R[1,0]-R[0,1])/(4*qw)
    return (qx,qy,qz,qw)
    
def Quat_to_RotMat(orientation):
    # convert quaternion to rotation matrix
    qx,qy,qz,qw = orientation
    
    R = np.empty((3,3))
    R[0,0] = 1.0-2.0*(qy*qy + qz*qz)
    R[0,1] = 2.0*(qx*qy - qw*qz)
    R[0,2] = 2.0*(qx*qz + qw*qy)
    R[1,0] = 2.0*(qx*qy + qw*qz)
    R[1,1] = 1.0-2.0*(qx*qx + qz*qz)
    R[1,2] = 2.0*(qy*qz - qw*qx)
    R[2,0] = 2.0*(qx*qz - qw*qy)
    R[2,1] = 2.0*(qy*qz + qw*qx)
    R[2,2] = 1.0-2.0*(qx*qx + qy*qy)
    
    return R
    
def GetHCPFCCTrans(self,fcc_neigh,fcc_index,fcc_ptm_query,hcp_index):
    # determine the transition matrix to go from FCC to HCP, used with coherent FCC-HCP interfaces
    angtol = 5e-2  #radians
    tol = 1e-5
    #setup query object for the HCP atom
    hcp_ptm_query = PTMNeighborFinder.Query(self.neigh_finder)
    hcp_ptm_query.find_neighbors(hcp_index, None)

    #store the reference orientation we are using, we need to keep using it for subsequent HCP atoms
    hcp_ref_orientation = hcp_ptm_query.orientation

    #set the first direction to be the one connecting the two atoms
    fcc1 = unit(fcc_neigh.ideal_vector)
    hcp1 = None
    for j in range(hcp_ptm_query.count):
        hcp_neigh = hcp_ptm_query[j]
        if hcp_neigh.index==fcc_index:
            hcp1_0 = np.asarray(hcp_neigh.ideal_vector)
            hcp1 = -unit(hcp_neigh.ideal_vector)
            break
    if hcp1 is None:
        return None, None
    vec_count = 1
    
    #loop over neighbors for both atoms until we find three noncoplanar directions
    for i in range(fcc_ptm_query.count):
        fcc_neigh = fcc_ptm_query[i]
        for j in range(hcp_ptm_query.count):
            hcp_neigh = hcp_ptm_query[j]
            #if the neighbor atom is common to the two atoms, use it to get a direction
            if fcc_neigh.index==hcp_neigh.index:
                if vec_count==1:
                    fcc2 = unit(fcc_neigh.ideal_vector)
                    #orthogonalize out the component along fcc1
                    hcp2 = unit(np.asarray(hcp_neigh.ideal_vector)-hcp1_0)
                    # hcp2 = unit(hcp_neigh.ideal_vector-np.dot(hcp_neigh.ideal_vector,hcp1)*hcp1)
                    #make sure they're not parallel with vectors 1
                    if abs(np.dot(fcc1,fcc2))>tol and abs(np.dot(hcp1,hcp2))>tol:
                        fcccross = unit(np.cross(fcc1,fcc2))
                        hcpcross = unit(np.cross(hcp1,hcp2))
                        vec_count = 2
                else:
                    fcc3 = unit(fcc_neigh.ideal_vector)
                    hcp3 = unit(np.asarray(hcp_neigh.ideal_vector)-hcp1_0)
                    # hcp3 = unit(hcp_neigh.ideal_vector-np.dot(hcp_neigh.ideal_vector,hcp1)*hcp1)
                    #make sure they're out of the plane formed by vectors 1 and 2
                    if abs(np.dot(fcc3,fcccross))>tol and abs(np.dot(hcp3,hcpcross))>tol:
                        vec_count = 3
                        break
        if vec_count==3:
            break
    if vec_count<3:
        return None, None
        #raise Exception('Unable to find three directions in GetHCPFCCTrans!')

    #compute the transformation matrix
    A = np.zeros((9,9))
    A[0,0:3] = hcp1
    A[1,3:6] = hcp1
    A[2,6:9] = hcp1
    A[3,0:3] = hcp2
    A[4,3:6] = hcp2
    A[5,6:9] = hcp2
    A[6,0:3] = hcp3
    A[7,3:6] = hcp3
    A[8,6:9] = hcp3
    b = np.concatenate((fcc1,fcc2,fcc3))
    Fvec = np.linalg.solve(A,b).T
    Ft = np.zeros((3,3))
    Ft[0,:] = Fvec[0:3]
    Ft[1,:] = Fvec[3:6]
    Ft[2,:] = Fvec[6:9]
    
    # print(Ft)
    # print(hcp_ref_orientation)
    
    return Ft,hcp_ref_orientation
    
    
    
    
    
    
    
    
    
    
    
     
 