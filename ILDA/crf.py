import ovito
from ovito.data import *
from ovito.modifiers import *

import numpy as np
from numpy.linalg import norm
import math

import ILDA.utilities
from ILDA.utilities import *

def polar_decomp(F):
    #perform the left polar decomposition of the specified deformation gradient
    #F = V*R where V is a stretch tensor and R is a rotation tensor
    
    #determine eigenvalues and eigenvectors of F^T*F
    V2 = np.matmul(F,np.transpose(F))
    w,v = np.linalg.eig(V2)
    
    #determine the stretch tensor
    Vp = np.diag(np.sqrt(w))
    V = np.matmul(np.transpose(v),np.matmul(Vp,v))
    
    #determine the rotation tensor
    R = np.matmul(np.linalg.inv(V),F)
    
    return V,R
    

def estimate_F(self, data: DataCollection):
    #compute the average deformation gradient for each grain using atoms within distF of the interface
    
    #get the list of neighbor atoms to be used when computing F 
    data.apply(CreateBondsModifier(cutoff=self.distF))
    bond_topology = data.particles.bonds.topology
    a1= bond_topology[:,0]
    a2= bond_topology[:,1]
    nbrs = np.unique(np.concatenate((a1[(data.particles['CIS'][a1]==0) & (data.particles['CIS'][a2]==1)],
                      a2[(data.particles['CIS'][a1]==1) & (data.particles['CIS'][a2]==0)])))
    #eliminate atoms which don't have the correct phase for each grain
    nbrs = nbrs[np.logical_or(np.logical_and(data.particles['Structure Type'][nbrs]==self.g1_s,data.particles['Grain'][nbrs]==self.g1_id),\
                              np.logical_and(data.particles['Structure Type'][nbrs]==self.g2_s,data.particles['Grain'][nbrs]==self.g2_id))]
    
    data.particles_.bonds_.create_property('Selection',data=np.ones(data.particles.bonds.count))
    data.apply(DeleteSelectedModifier(operate_on={'bonds'}))
    
            
    #compute the deformation gradients for mapping to the coherent reference state
    #loop over atoms in Grain A
    FsumA = np.zeros((3,3))
    FsumB = np.zeros((3,3))
    NA = 0
    NB = 0
    yield "Estimating FA and FB..."
    for i in range(len(nbrs)):
        nbri = nbrs[i]
        if data.particles['Grain'][nbri]==self.g1_id or data.particles['Grain'][nbri]==self.g2_id:
            Fi = totalDeformationGradPTM(self, nbri,data.particles['Structure Type'][nbri],data.particles['Grain'][nbri])
            if data.particles['Grain'][nbri]==self.g1_id:
                FsumA += Fi
                NA += 1
            elif data.particles['Grain'][nbri]==self.g2_id:
                FsumB += Fi
                NB += 1
        yield (i/len(nbrs))
    self.FA = FsumA/NA
    self.FB = FsumB/NB
    
    print("Used",NA,"and",NB,"atoms to estimate FA and FB, respectively.")
    
    VA,RA = polar_decomp(self.FA)
    VB,RB = polar_decomp(self.FB)
    
    print("Estimated coherent reference frame (with polar decomposition)")
    print("FA=VA*RA:")
    for i in range(3):
        if i==1:
            print(self.FA[i,:],"=",VA[i,:],"*",RA[i,:])
        else:
            print(self.FA[i,:]," ",VA[i,:]," ",RA[i,:])
    print("FB=VB*RB:")
    for i in range(3):
        if i==1:
            print(self.FB[i,:],"=",VB[i,:],"*",RB[i,:])
        else:
            print(self.FB[i,:]," ",VB[i,:]," ",RB[i,:])

def compute_F(self,xA,yA,xB,yB,EcohA,EcohB):
    #compute the deformation gradients mapping to the CRF using the given vectors and coherency strains

    if abs(np.dot(xA,yA))>1e-6:
        raise Exception('xA and yA are not orthogonal!')
    if abs(np.dot(xB,yB))>1e-6:
        raise Exception('xB and yB are not orthogonal!')

    zA = np.cross(xA,yA)
    zB = np.cross(xB,yB)

    xA = xA/norm(xA)
    yA = yA/norm(yA)
    zA = zA/norm(zA)

    xB = xB/norm(xB)
    yB = yB/norm(yB)
    zB = zB/norm(zB)

    RA = np.vstack((xA,yA,zA))
    RB = np.vstack((xB,yB,zB))

    FA = np.matmul((np.identity(3)+EcohA),RA)
    FB = np.matmul((np.identity(3)+EcohB),RB)
    
    self.FA = FA
    self.FB = FB
    
    # print("User specified coherent reference frame")
    # print("FA:")
    # print(FA)
    # print("FB:")
    # print(FB)
    
    #use the rotation matrix to get the reference orientation
    self.g1_orientation=RotMat_to_Quat(RA)
    self.g2_orientation=RotMat_to_Quat(RB)