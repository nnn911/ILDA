import random

import ovito
from ovito.data import *
from ovito.modifiers import *
from ovito.vis import *

import numpy as np
from numpy.linalg import norm
import math

import ILDA.cis
from ILDA.cis import *

import ILDA.utilities
from ILDA.utilities import *

import ILDA.surface
from ILDA.surface import *

import ILDA.circuit
from ILDA.circuit import *

import ILDA.plane
from ILDA.plane import *

import ILDA.crf
from ILDA.crf import *

import importlib
#reload in case we edited it in real time
importlib.reload(ILDA.cis)
importlib.reload(ILDA.utilities)
importlib.reload(ILDA.circuit)
importlib.reload(ILDA.surface)
importlib.reload(ILDA.plane)
importlib.reload(ILDA.crf)

import sys

#colors for dislocation segments and nodes
colorlist = np.asarray(((0,         0,    0.6000),
             (0,         0,    0.8000),
             (0,         0,    1.0000),
             (0,    0.2000,    1.0000),
             (0,    0.4000,    1.0000),
             (0,    0.6000,    1.0000),
             (0,    0.8000,    1.0000),
             (0,    1.0000,    1.0000),
        (0.2000,    1.0000,    0.8000),
        (0.4000,    1.0000,    0.6000),
        (0.6000,    1.0000,    0.4000),
        (0.8000,    1.0000,    0.2000),
        (1.0000,    1.0000,         0),
        (1.0000,    0.8000,         0),
        (1.0000,    0.6000,         0),
        (1.0000,    0.4000,         0),
        (1.0000,    0.2000,         0),
        (1.0000,         0,         0),
        (0.8000,         0,         0),
        (0.6000,         0,         0)))
Ncolors = colorlist.shape[0]

class ILDA:
    """Main class for ILDA, containing the constructor, main function (find_defects) and driver function (run) for the modifier"""
    
    def __init__(self, data: DataCollection,selection_only,atomA,atomB,cis_tol,extract_lines,aA,aB,Rsphere,btol,angtol,distF,estimateF,xA,yA,xB,yB,EcohA,EcohB):
        """ Constructor """        
        
        #make sure the user has called the Coincidence Sites modifier
        # if 'CIS' not in data.particles:
        #     raise RuntimeError('User needs to call Coincidence Sites modifier before this one!')
        
        self.cis_tol = cis_tol
        self.extract_lines = extract_lines
        self.aA=aA
        self.aB=aB
        self.Rsphere = Rsphere
        self.btol = btol
        self.angtol = angtol*math.pi/180
        self.distF = distF
        self.estimateF = estimateF
        self.xA = np.array(xA)
        self.yA = np.array(yA)
        self.xB = np.array(xB)
        self.yB = np.array(yB)
        self.EcohA = np.array(EcohA)
        self.EcohB = np.array(EcohB)
        
        #determine grains based on fiducial atoms
        self.g1_id=data.particles['Grain'][data.particles['Particle Identifier']==atomA].squeeze()
        self.g2_id=data.particles['Grain'][data.particles['Particle Identifier']==atomB].squeeze()
        
        if self.g1_id==self.g2_id:
            raise Exception("atomA and atomB are both part of grain",self.g1_id)
        
        self.g1_s=data.tables['grains']['Structure Type'][self.g1_id-1]
        self.g2_s=data.tables['grains']['Structure Type'][self.g2_id-1]
        #use grain orientations for the reference orientations, will get updated if the user specified the CRF
        self.g1_orientation=data.tables['grains']['Orientation'][self.g1_id-1]
        self.g2_orientation=data.tables['grains']['Orientation'][self.g2_id-1]
        #store the grain IDs used here so we know them later
        data.attributes['Grain A'] = self.g1_id
        data.attributes['Grain B'] = self.g2_id
        
        # Set up the PTMNeighborFinder and a query object:
        self.neigh_finder = ovito.nonpublic.PTMNeighborFinder(False)#PTMNeighborFinder(data)        #Need to understand PTMNeighborFinder!!!
        self.neigh_finder.prepare(data.particles['Position'], data.particles['Structure Type'], data.particles['Orientation'], data.particles['Correspondences'], data.cell)
        self.ptm_query = ovito.nonpublic.PTMNeighborFinder.Query(self.neigh_finder)#PTMNeighborFinder.Query(self.neigh_finder)

        # Save some important input data for later.
        self.particle_count = data.particles.count
        self.structure_types = data.particles['Structure Type']
        self.positions = data.particles['Position']
        #self.def_gradient = data.particles['Elastic Deformation Gradient']
        self.particle_identifier = data.particles['Particle Identifier']
        self.atomic_dist= data.particles['Interatomic Distance']
        # self.cis = data.particles['CIS']
        #self.mesh=data.surfaces['surface']
        # self.finder = CutoffNeighborFinder(cutoff, data)
        self.grain= data.particles['Grain']
        if data.particles.selection and selection_only:
            self.selection = data.particles.selection
        else:
            self.selection = np.ones(data.particles.count,dtype=int)
        
        # PBC      
        self.h_matrix= data.cell[:, 0:3]
        
        number_of_grains = data.tables['grains'].count        
    
        
        
        
        #cis stuff
        self.color = data.particles_.create_property('Color')
        self.cis = data.particles_.create_property('CIS', data=np.zeros(data.particles.count,dtype=int))
        self.cisnbrs = -np.ones((self.particle_count,2), dtype=int)
        self.cis_ideal_vecs = np.zeros((self.particle_count,2,3), dtype=float)
        self.cis_spatial_vecs = np.zeros((self.particle_count,2,3), dtype=float)
        
        # Create output particle property, which is used to visualize vector arrows.
        # self.output_vectors = data.particles_.create_property('My vectors', dtype=float, components=3)
#         self.output_vectors1 = data.particles_.create_property('My vectors', dtype=float, components=3)
#         self.color = data.particles_.create_property('Color')



    #these functions are used to insert line segments (bonds)    
    def get_b_id(self,bnorm,uniquebmags):
        if bnorm>self.btol:
            return (np.abs(uniquebmags - bnorm)).argmin()
        return -1

    def add_segment(self,data:DataCollection,bond_topology,bond_burg,bond_step,bond_plane,bond_color,bond_id,atom1,atom2,pcount,pos1,pos2,b,bnorm,h,plane,bcolor,bid):
        """Add a line defect segment to the bonds structure."""
        if atom1>=pcount:
            data.particles_.create_particle(pos1)
            data.particles_['Particle Type'][pcount]=newid
            data.particles_['Particle Identifier'][pcount]=atom1+1
            pcount+=1
        if atom2>=pcount:
            data.particles_.create_particle(pos2)
            data.particles_['Particle Type'][pcount]=newid
            data.particles_['Particle Identifier'][pcount]=atom2+1
            pcount+=1

        bond_topology.append((atom1,atom2))
        bond_color.append(bcolor)
        bond_burg.append(b)
        bond_step.append(h)
        bond_plane.append(plane)
        bond_id.append(bid+1)
        # pcount+=2
        return bond_topology,bond_burg,bond_step,bond_plane,bond_color,bond_id,pcount
        
        
        
    def segment_atom(self,data:DataCollection,edge1,edge2,midedge_atoms,pcount):
        #if edge1 is blank, add a new particle, otherwise check to see if it exists
        if edge1==-1:
            atom1 = -1
        elif edge1:     
            if midedge_atoms[edge1]>0:
                atom1 = midedge_atoms[edge1]
            else:
                atom1 = pcount
                midedge_atoms[edge1]=pcount
                opp_edge = data.surfaces['surface'].topology.opposite_edge(edge1)
                if opp_edge!=-1:
                    midedge_atoms[opp_edge]=pcount
        else:
            atom1 = pcount
        
        #if atom1 is new, we have to add two new atoms    
        if midedge_atoms[edge2]>0:
            atom2 = midedge_atoms[edge2]
        elif atom1==pcount:
            atom2 = pcount+1
            midedge_atoms[edge2]=pcount+1
            opp_edge = data.surfaces['surface'].topology.opposite_edge(edge2)
            if opp_edge!=-1:
                midedge_atoms[opp_edge]=pcount+1
        else:
            atom2 = pcount
            midedge_atoms[edge2]=pcount
            opp_edge = data.surfaces['surface'].topology.opposite_edge(edge2)
            if opp_edge!=-1:
                midedge_atoms[opp_edge]=pcount
            
        return atom1,atom2,midedge_atoms
    
    def find_defects(self, data:DataCollection):
        """Uses the previously constructed surface mesh to find all line defects"""
        
        mesh = data.surfaces['surface']
        #------------------------------------------------------
        #Construction of Burgers circuits
        #------------------------------------------------------
        bvec  = np.zeros((mesh.topology.edge_count,4))
        plane = np.zeros((mesh.topology.edge_count,3))
        step  = np.zeros((mesh.topology.edge_count))
        #---------------------------------
            
        #loop over faces, and then edges to construct Burgers circuits for each
        yield "Constructing Burgers circuits..."
        printed_mesh_warning = False
        for m in range(mesh.topology.face_count):
            edge1 = mesh.topology.first_face_edge(m)
            edge2 = mesh.topology.next_face_edge(edge1)
            edge3 = mesh.topology.prev_face_edge(edge1)
            for edge in (edge1,edge2,edge3):
                #first check if we already encountered the opposite half edge, and if so copy the Burgers vector over
                if bvec[mesh.topology.opposite_edge(edge),3]>0:
                    #flip the sign since the sense vector is flipped
                    bvec[edge,0:3] = -bvec[mesh.topology.opposite_edge(edge),0:3]
                    bvec[edge,3] = bvec[mesh.topology.opposite_edge(edge),3]
                else:                            
                    vertex1 = mesh.topology.first_edge_vertex(edge)
                    vertex2 = mesh.topology.second_edge_vertex(edge)            
                    atom1 = mesh.vertices['Index'][vertex1]
                    atom2 = mesh.vertices['Index'][vertex2]
                    #skip if out of the selection
                    if 'Selection' in data.particles and (data.particles.selection[atom1]==0 or data.particles.selection[atom2]==0):
                        continue;
                    #make sure CIS atoms bound the edge
                    if self.cis[atom1]==1 and self.cis[atom2]==1:
                        #perform the Burgers circuit
                        output= BurgersCircuit(self,atom1, atom2)
                        if not output is None:
                            #all information is stored based on the vertex ids for the edge
                            bvec[edge,0:3],_,_ =output
                            ##################################################3
                            #hack to introduce additonal variants and Burgers vectors....
                            # if m>mesh.topology.face_count/4 and m<mesh.topology.face_count/2:
                            #     bvec[edge,1] += 1.04
                            # elif m>=mesh.topology.face_count/2:
                            #     bvec[edge,0] += -1.04
                            # bvec[edge,0:3] += random.uniform(-0.05, 0.05)
                                #############################################
                            # print(bvec[edge,0:3])
                            #save the norm too, we use it a lot
                            bvec[edge,3] = norm(bvec[edge,0:3])
                    elif ((self.cis[atom1]==1 and self.cis[atom2]==0) or (self.cis[atom1]==0 and self.cis[atom2]==1)) and not printed_mesh_warning:
                        print("Warning: mesh edge connects CIS atom to non-CIS atom, Rsphere probably needs to be increased")
                        printed_mesh_warning = True
            yield (m/mesh.topology.face_count)
            
        
        #unique list of Burgers vector magnitudes, assign a color to each
        bbins = np.arange(self.btol,np.amax(bvec[:,3])+self.btol,self.btol)
        bbinmids = (bbins[:-1]+bbins[1:])/2
        bhistogram = np.histogram(bvec[:,3],bins=bbins)
        uniquebmags = np.flip(bbinmids[bhistogram[0]>0])

        Nb = len(uniquebmags)
        #if there are more than Ncolors unique Burgers vectors, just use black for all
        if Nb<=Ncolors and Nb>0:
            bcolors = colorlist[np.arange(0,Ncolors,np.floor(Ncolors/Nb),dtype=int)]
        else:
            uniquebmags = np.zeros(1)
            bcolors = np.zeros((3,1))
            
        #now that we know all of the Burgers vector content, determine the habit planes and step heights
        yield "Determining step heights..."
        for m in range(mesh.topology.face_count):
            edge1 = mesh.topology.first_face_edge(m)
            edge2 = mesh.topology.next_face_edge(edge1)
            edge3 = mesh.topology.prev_face_edge(edge1)
            for edge in (edge1,edge2,edge3):
                #first check if we already encountered this edge, and if so copy the step height over
                if bvec[edge,3]>self.btol:
                    if step[mesh.topology.opposite_edge(edge)]>0:
                        plane[edge,0:3] = plane[mesh.topology.opposite_edge(edge),0:3]
                        step[edge] = step[mesh.topology.opposite_edge(edge)]
                    else:
                        vertex1 = mesh.topology.first_edge_vertex(edge)
                        vertex2 = mesh.topology.second_edge_vertex(edge)
                        plane[edge,0:3], step[edge] = Find_TerracePlane2(self, data, vertex1, vertex2, bvec)
            yield (m/mesh.topology.face_count)
        
        #------------------------------------------------------
        #Insert line defect segments
        #------------------------------------------------------
        #add a node particle type
        type_idlist = []
        #if Particle Type attribute does not exist, add it
        if not hasattr(data.particles.particle_types, 'types'):
            data.particles_.create_property('Particle Type')
            type_idlist.append(0)
        else:
            for t in data.particles.particle_types.types: type_idlist.append(t.id)
        global newid
        newid = max(type_idlist)+1
        data.particles_.create_property('Particle Type').types.append(ParticleType(id = newid,name = 'Node', color = (0.0,0.0,0.0), radius = 0.1))
        
        #initialize tuples for storing segment info
        i=0
        bond_topology=[]
        bond_color = []
        bond_burg=[]
        bond_step=[]
        bond_plane=[]
        bond_id=[]
        
        centroid = np.matmul(self.h_matrix,((0.5,0.5,0.5)))
        
        #array to keep track of edges where nodes have been inserted
        midedge_atoms = np.zeros(mesh.topology.edge_count,dtype=int)
        
        #initialize arrays for computing average line defect quantities
        bprotos = [[] for _ in range(Nb)]    #list of lists
        bsums = [[] for _ in range(Nb)]
        bsum2s = [[] for _ in range(Nb)]
        hsums = [[] for _ in range(Nb)]
        hmaxs = [[] for _ in range(Nb)]
        hmins = [[] for _ in range(Nb)]
        Lsums = [[] for _ in range(Nb)]
                
        #loop over mesh faces
        printed_bcon_warning = False
        pcount=self.particle_count
        yield "Inserting line segments..."
        for m in range(mesh.topology.face_count):
            edge1 = mesh.topology.first_face_edge(m)
            edge2 = mesh.topology.next_face_edge(edge1)
            edge3 = mesh.topology.prev_face_edge(edge1)
            
            vertex1 = mesh.topology.first_edge_vertex(edge1)
            vertex2 = mesh.topology.first_edge_vertex(edge2)
            vertex3 = mesh.topology.first_edge_vertex(edge3)
            
            atom1 = mesh.vertices['Index'][vertex1]
            atom2 = mesh.vertices['Index'][vertex2]
            atom3 = mesh.vertices['Index'][vertex3]
            
            #skip the face if not all vertices are CIS atoms
            if self.cis[atom1]!=1 or self.cis[atom2]!=1 or self.cis[atom3]!=1:
                continue
            
            #skip if all atoms aren't selected
            if 'Selection' in data.particles and (data.particles.selection[atom1]==0 or data.particles.selection[atom2]==0 or data.particles.selection[atom3]==0):
                continue;
            
            #check conservation of Burgers vector
            #line sense is based on segments pointing into the triangle
            b1 = bvec[edge1,0:3]
            b2 = bvec[edge2,0:3]
            b3 = bvec[edge3,0:3]
            h1 = step[edge1]
            h2 = step[edge2]
            h3 = step[edge3]
            plane1 = plane[edge1,0:3]
            plane2 = plane[edge2,0:3]
            plane3 = plane[edge3,0:3]
            b_sum_norm= norm(b1+b2+b3)
            # print(b_sum_norm)
            if b_sum_norm>self.btol:
                mesh.vertices['Color'][vertex1] = (1,0,0)
                mesh.vertices['Color'][vertex2] = (1,0,0)
                mesh.vertices['Color'][vertex3] = (1,0,0)
                if not printed_bcon_warning:
                    print("Warning: some mesh faces failed Burgers vector conservation, they are highlighted red.")
                    printed_bcon_warning = True

            #insert line segments for edges with nonzero Burgers vectors
            bnorm1 = bvec[edge1,3]
            bnorm2 = bvec[edge2,3]
            bnorm3 = bvec[edge3,3]
            #print(bnorm1,bnorm2,bnorm3,self.btol)
            if bnorm1>self.btol or bnorm2>self.btol or bnorm3>self.btol:
                #add a node in the middle
                posv1 = mesh.vertices['Position'][vertex1]
                posv2 = posv1 + pbc_vec(self.h_matrix,mesh.vertices['Position'][vertex2]-posv1)
                posv3 = posv1 + pbc_vec(self.h_matrix,mesh.vertices['Position'][vertex3]-posv1)
                pos1 = (posv1 + posv2)/2
                pos2 = (posv2 + posv3)/2
                pos3 = (posv1 + posv3)/2
                
                #determine which Burgers vector each edge belongs to
                bid1 = self.get_b_id(bnorm1,uniquebmags)
                bid2 = self.get_b_id(bnorm2,uniquebmags)
                bid3 = self.get_b_id(bnorm3,uniquebmags)
                
                L1, L2, L3 = 0, 0, 0
                if bnorm1>self.btol and bnorm2>self.btol and bnorm3>self.btol:
                    mid = (pos1 + pos2 + pos3)/3

                    L1 = norm(pos1-mid)
                    atom1,atom2,midedge_atoms = self.segment_atom(data,[],edge1,midedge_atoms,pcount)
                    bond_topology,bond_burg,bond_step,bond_plane,bond_color,bond_id,pcount = self.add_segment(data,bond_topology,bond_burg,bond_step,bond_plane,bond_color,bond_id,atom1,atom2,pcount,mid,pos1,-b1,bnorm1,h1,plane1,bcolors[bid1],bid1)

                    L2 = norm(pos2-mid)
                    _,atom2,midedge_atoms = self.segment_atom(data,-1,edge2,midedge_atoms,pcount)
                    bond_topology,bond_burg,bond_step,bond_plane,bond_color,bond_id,pcount = self.add_segment(data,bond_topology,bond_burg,bond_step,bond_plane,bond_color,bond_id,atom1,atom2,pcount,mid,pos2,-b2,bnorm2,h2,plane2,bcolors[bid2],bid2)

                    L3 = norm(pos3-mid)
                    _,atom2,midedge_atoms = self.segment_atom(data,-1,edge3,midedge_atoms,pcount)
                    bond_topology,bond_burg,bond_step,bond_plane,bond_color,bond_id,pcount = self.add_segment(data,bond_topology,bond_burg,bond_step,bond_plane,bond_color,bond_id,atom1,atom2,pcount,mid,pos3,-b3,bnorm3,h3,plane3,bcolors[bid3],bid3)
                elif bnorm1>self.btol and bnorm2>self.btol:
                    L1 = norm(pos1-pos2)
                    atom1,atom2,midedge_atoms = self.segment_atom(data,edge1,edge2,midedge_atoms,pcount)
                    bond_topology,bond_burg,bond_step,bond_plane,bond_color,bond_id,pcount = self.add_segment(data,bond_topology,bond_burg,bond_step,bond_plane,bond_color,bond_id,atom1,atom2,pcount,pos1,pos2,b1,bnorm1,h1,plane1,bcolors[bid1],bid1)

                elif bnorm2>self.btol and bnorm3>self.btol:
                    L2 = norm(pos2-pos3)
                    atom1,atom2,midedge_atoms = self.segment_atom(data,edge2,edge3,midedge_atoms,pcount)
                    bond_topology,bond_burg,bond_step,bond_plane,bond_colo,bond_id,pcount = self.add_segment(data,bond_topology,bond_burg,bond_step,bond_plane,bond_color,bond_id,atom1,atom2,pcount,pos2,pos3,b2,bnorm2,h2,plane2,bcolors[bid2],bid2)

                elif bnorm1>self.btol and bnorm3>self.btol:
                    L3 = norm(pos1-pos3)
                    atom1,atom2,midedge_atoms = self.segment_atom(data,edge3,edge1,midedge_atoms,pcount)
                    bond_topology,bond_burg,bond_step,bond_plane,bond_color,bond_id,pcount = self.add_segment(data,bond_topology,bond_burg,bond_step,bond_plane,bond_color,bond_id,atom1,atom2,pcount,pos3,pos1,b3,bnorm3,h3,plane3,bcolors[bid3],bid3)

                #add to the line statistics, all Burgers vectors are ordered and signed to match the magnitudes and signs of the first entry for each bid
                for bi,bidi,Li,hi in zip((b1,b2,b3),(bid1,bid2,bid3),(L1,L2,L3),(h1,h2,h3)):
                    if bidi>=0 and Li>0:
                        add_new = False
                        #if this is the first line segment for this bid, add it
                        if not Lsums[bidi]:
                            add_new = True
                            sign = 1
                            j = 0
                        #otherwise, compare this b with all other Burgers vector prototypes
                        #if this b is not parallel to any of them, add it to the list of prototypes 
                        else:
                            for j in range(len(bprotos[bidi])):
                                is_parallel,sign = parallel_test(bi,bprotos[bidi][j],self.angtol)
                                if is_parallel:
                                    break
                            if not is_parallel:
                                add_new = True
                                sign = 1
                                j = j+1
                        if add_new:
                            bprotos[bidi].append(bi)
                            bsums[bidi].append(0)
                            bsum2s[bidi].append(0)
                            hsums[bidi].append(0)
                            Lsums[bidi].append(0)
                            hmaxs[bidi].append(-1e10)
                            hmins[bidi].append(1e10)
                        bstat = sign*bi
                        bsums[bidi][j] += Li*bstat 
                        bsum2s[bidi][j] += Li*bstat*bstat
                        hsums[bidi][j] += Li*hi
                        Lsums[bidi][j] += Li
                        hmaxs[bidi][j] = max(hmaxs[bidi][j],hi)
                        hmins[bidi][j] = min(hmins[bidi][j],hi)

            yield (m/mesh.topology.face_count)
            
        #now "prune" the results to make sure the variants are truly distinct from one another
        #sometimes we falsly identify unique variants above if we happen to choose an unrepresentative prototype
        for i in range(Nb):
            j = 0
            while j<len(bprotos[i]):
                bavgj = bsums[i][j]/Lsums[i][j]
                k = j+1
                while k<len(bprotos[i]):
                    bavgk = bsums[i][k]/Lsums[i][k]
                    is_parallel,sign = parallel_test(bavgj,bavgk,self.angtol)
                    #if the average vectors are parallel, merge them together and update the Burgers IDs
                    if is_parallel:
                        #transfer info from variant k to variant j
                        bsums[i][j] += sign*bsums[i][k] 
                        bsum2s[i][j] += bsum2s[i][k] 
                        hsums[i][j] += hsums[i][k]
                        Lsums[i][j] += Lsums[i][k]
                        hmaxs[i][j] = max(hmaxs[i][j],hmaxs[i][k])
                        hmins[i][j] = min(hmins[i][j],hmins[i][k])
                        #delete variant k
                        bsums[i].pop(k)
                        bsum2s[i].pop(k)
                        hsums[i].pop(k)
                        Lsums[i].pop(k)
                        hmaxs[i].pop(k)
                        hmins[i].pop(k)
                        bprotos[i].pop(k)
                    else:
                        k += 1
                j += 1
        
        #insert all of the segments, if any were found    
        if bond_topology:
            data.particles.bonds = Bonds()
            data.particles.bonds.create_property('Topology', data=bond_topology)
            data.particles.bonds.create_property('Color', data=bond_color)
            data.particles.bonds.create_property('Burgers Vector', data=bond_burg)
            data.particles.bonds.create_property('Step Height', data=bond_step)
            data.particles.bonds.create_property('Terrace Plane', data=bond_plane)
            data.particles.bonds.create_property('Burgers ID', data=bond_id)
            data.particles.bonds.create_property('Width', data=np.ones(len(bond_step)))
            bonds = data.particles_.bonds_.topology_
            pbc_vectors = data.particles.delta_vector(bonds[:,1], bonds[:,0], data.cell, True)[1]
            data.particles_.bonds_.create_property("Periodic Image", data=pbc_vectors)
            data.apply(WrapPeriodicImagesModifier())
            
        #if there are lines everywhere, there is probably something wrong...
        if i>3*0.9*mesh.topology.face_count:
            print("WARNING: Excessive number of line defects identified, the coherent reference state is probably not coherent.")

        #compute and print averages for the identified line defects
        print("Found",Nb,"unique Burgers vector magnitudes which differ by more than btol=",self.btol)
        print("ID\tVariant\tLength\tAvg b\tAvg Burgers vector\tStd of Burgers vector\tAvg h\tMin h\tMax h")
        np.set_printoptions(formatter={'float': '{: .3f}'.format})
        Ltotal = 0
        for i in range(Nb):
            for j in range(len(bprotos[i])):
                bavg = bsums[i][j]/Lsums[i][j]  
                bavgnorm = norm(bavg)
                bstd = np.sqrt(np.absolute(Lsums[i][j]*bsum2s[i][j]-bsums[i][j]*bsums[i][j]))/Lsums[i][j]
                havg = hsums[i][j]/Lsums[i][j]
                Li = Lsums[i][j]
                Ltotal += Li
                hmax = hmaxs[i][j]
                hmin = hmins[i][j]
                print(i+1,"\t",j+1,"\t",f"{Li:.3f}\t",f"{bavgnorm:.3f}\t",bavg,"\t",bstd,"\t",f"{havg:.3f}\t",f"{hmin:.3f}\t",f"{hmax:.3f}")
        print("Total line length =",f"{Ltotal:.3f}")
    
    def run(self, data: DataCollection):
        """Driver function for ILDA"""        
        #if we are changing the reference orientation due to user specified CRF, need to compute that before finding CIS atoms
        if not self.estimateF:
            compute_F(self,self.xA,self.yA,self.xB,self.yB,self.EcohA,self.EcohB)
   
        #find the coincidence sites
        yield from Find_CIS_v2(self,data)
                    
        #if the user wants to extract lines, call everything else
        if self.extract_lines:  
            #estimate the coherent reference state if it wasn't given above
            if self.estimateF:
                yield from estimate_F(self,data)
            else:
                print("User specified coherent reference frame")
                print("FA:")
                print(self.FA)
                print("FB:")
                print(self.FB)   
                   
            #create the surface mesh for the interface       
            yield from create_mesh_v2(self,data)
            
            #construct Burgers circuits and find lines    
            yield from self.find_defects(data)
        
        #uncomment if we want to show Burgers circuits
        # vector_vis=VectorVis(alignment=VectorVis.Alignment.Center, color=(1.0, 0.0, 0.4),flat_shading=False, width=0.1)
#         self.output_vectors.vis = vector_vis