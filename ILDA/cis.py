import ovito
from ovito.data import *
from ovito.modifiers import *
import numpy as np

import ILDA.utilities
from ILDA.utilities import *
    
def Find_CIS_v2(self, data: DataCollection):
    """
    Identify CIS atoms using two approaches:
    1) find atoms of unknown structure which are neighbors of crystalline atoms, one from crystals A and B
    2) extrapolate from the edges of crystals A and B and see if any atoms are within cis_tol of these positions
    
    """

    #find 18 nearest neighbors for each atom in grains A and B which have the correct phase
    #(max number used in PTM 16 + 10 to be safe)
    finder = NearestNeighborFinder(26, data)
    g1g2atoms = np.where(np.logical_and(self.selection,
                         np.logical_or(np.logical_and(self.structure_types==self.g1_s,self.grain==self.g1_id),
                                       np.logical_and(self.structure_types==self.g2_s,self.grain==self.g2_id))))[0]
    neigh_idx,_ = finder.find_all(indices=g1g2atoms)
    
    #find atoms in each grain which have at least one neighbor of type other
    g1g2candidates = g1g2atoms[np.sum(self.structure_types[neigh_idx]==0,axis=1)>0]
    
    ptm_query = PTMNeighborFinder.Query(self.neigh_finder)
    ptm_query_ref = PTMNeighborFinder.Query(self.neigh_finder)
    
    #loop over an atom from both grains to collect together all ideal lattice vectors, rotated into the crystal orientation
    ideal_vecs = [[] for _ in range(2)]
    rot_ideal_vecs = [[] for _ in range(2)]
    grain_IDs = [self.g1_id,self.g2_id]
    grain_orientations = [data.tables['grains']['Orientation'][self.g1_id-1],data.tables['grains']['Orientation'][self.g2_id-1]]
    grain_ref_orientations =  [self.g1_orientation,self.g2_orientation]
    grain_Rs = [Quat_to_RotMat(grain_orientations[0]),Quat_to_RotMat(grain_orientations[1])]
    grain_scales = [scalePTMInteratomicDistance(self.g1_s,1)*scalePTMVec(self.g1_s,1),scalePTMInteratomicDistance(self.g2_s,1)*scalePTMVec(self.g2_s,1)]
    for grain_ind in range(len(grain_IDs)):        
        for m in range(self.particle_count):
            if self.grain[m] == grain_IDs[grain_ind]:
                ptm_query.find_neighbors(m, grain_orientations[grain_ind])
                ptm_query_ref.find_neighbors(m, grain_ref_orientations[grain_ind])
                for jj in range(ptm_query.count):
                    neigh = ptm_query[jj]
                    neigh_ref = ptm_query_ref[jj]
                    #use the grain orientation to get the spatial vector, and the reference orientation to get the lattice vector
                    vectorjj = np.array(neigh.ideal_vector)
                    vectorjj_ref = np.array(neigh_ref.ideal_vector)
                    ideal_vecs[grain_ind].append(vectorjj_ref)
                    rot_ideal_vecs[grain_ind].append(np.matmul(grain_Rs[grain_ind],vectorjj*grain_scales[grain_ind]))
                break
    
    #loop over atoms to identify CIS atoms
    yield "Finding coincidence sites..."
    for i in range(len(g1g2candidates)): # range (self.particle_count):
        m = g1g2candidates[i]
        
        #assign the reference orientation and grain index
        if self.grain[m] == self.g1_id:
            ref_orientation=self.g1_orientation    #ref_orientationFCC
            grain_ind = 0
        
        elif self.grain[m] == self.g2_id:
            ref_orientation=self.g2_orientation    #ref_orientationBCC
            grain_ind = 1
         
        # ptm_query = PTMNeighborFinder.Query(self.neigh_finder)
        ptm_query.find_neighbors(m, ref_orientation)
               
        for jj in range(ptm_query.count):
            neigh = ptm_query[jj]
            
            #if the neighbor atom is not type Other, skip it
            if self.structure_types[neigh.index] != PolyhedralTemplateMatchingModifier.Type.OTHER:
                continue
            
            #if we already explored this atom for this grain, skip it
            if self.cisnbrs[neigh.index,grain_ind]>0:
                continue
    
            #add the atom to the appropriate candidate list
            self.cisnbrs[neigh.index,grain_ind] = m
            self.cis_ideal_vecs[neigh.index,grain_ind,:] = -np.array(neigh.ideal_vector)
            self.cis_spatial_vecs[neigh.index,grain_ind,:] = -np.array(neigh.delta)
            
            if self.cis_tol>0:
                #loop over its neighbors and see if any fall on lattice sites 
                for neigh2 in finder.find(neigh.index):
            
                    #if the atom is not type Other, skip it
                    if self.structure_types[neigh2.index] != PolyhedralTemplateMatchingModifier.Type.OTHER:
                        continue
            
                    #if we already explored this atom for this grain, skip it
                    if self.cisnbrs[neigh2.index,grain_ind]>0:
                        continue
                
                    for k in range(len(ideal_vecs[grain_ind])):
                        nbr_vec = self.atomic_dist[m]*rot_ideal_vecs[grain_ind][k]
                        #check if the neighbor is within cis_tol of any lattice site
                        if np.linalg.norm(neigh2.delta-nbr_vec)<self.cis_tol:
                            #add the atom to the appropriate candidate list
                            self.cisnbrs[neigh2.index,grain_ind] = m
                            self.cis_ideal_vecs[neigh2.index,grain_ind,:] = -np.array(neigh.ideal_vector)-ideal_vecs[grain_ind][k]
                            self.cis_spatial_vecs[neigh2.index,grain_ind,:] = -np.array(neigh.delta)-np.array(neigh2.delta)
            
                            #loop over its neighbors and see if any fall on lattice sites
                            for neigh3 in finder.find(neigh2.index):

                                #if the atom is not type Other, skip it
                                if self.structure_types[neigh3.index] != PolyhedralTemplateMatchingModifier.Type.OTHER:
                                    continue

                                #if we already explored this atom for this grain, skip it
                                if self.cisnbrs[neigh3.index,grain_ind]>0:
                                    continue

                                for l in range(len(ideal_vecs[grain_ind])):
                                    nbr_vec = self.atomic_dist[m]*rot_ideal_vecs[grain_ind][l]
                                    #check if the neighbor is within cis_tol of any lattice site
                                    if np.linalg.norm(neigh3.delta-nbr_vec)<self.cis_tol:
                                        #add the atom to the appropriate candidate list
                                        self.cisnbrs[neigh3.index,grain_ind] = m
                                        self.cis_ideal_vecs[neigh3.index,grain_ind,:] = -np.array(neigh.ideal_vector)-ideal_vecs[grain_ind][k]-ideal_vecs[grain_ind][l]
                                        self.cis_spatial_vecs[neigh3.index,grain_ind,:] = -np.array(neigh.delta)-np.array(neigh2.delta)-np.array(neigh3.delta)
                                        break
                                break
                        
                        #if none of the neighbors are on lattice sites, just do a double extrapolation to see if we hit anything
        yield (i/len(g1g2candidates))
    
    #find atoms which have entries > -1 for both grains and add CIS info to their properties
    listcis = np.where(np.logical_and(self.cisnbrs[:,0]>=0,self.cisnbrs[:,1]>=0))[0]
    self.color[listcis] = (1.0, 1.0, 0.0)
    self.cis[listcis] = 1
    
    print("Found",sum(self.cis),"CIS atoms")