import ovito
from ovito.data import *
from ovito.modifiers import *

import numpy as np
from numpy.linalg import norm

import ILDA.utilities
from ILDA.utilities import *

def Find_TerracePlane(self, start_index,end_index):
    #deprecated function for determining the habit plane

    ptm_query = PTMNeighborFinder.Query(self.neigh_finder)      
    
    for pindex in [start_index,end_index]:
        
        a1=np.zeros(3)
        a2=np.zeros(3)            
        i=0
        ptm_query.find_neighbors(pindex, None)
        for j in range(ptm_query.count):
            neigh = ptm_query[j] 
            if not self.cis[neigh.index]:#not CIS atoms: 
                continue                 
            
            if i==0:
                a1=np.array(neigh.delta)                
            else:    
                a2=np.array(neigh.delta) 
                if abs(abs(dotproduct(a1, a2) / (length(a1) * length(a2)))-1) > 0.01:
                    break                      
            i=i+1

        if pindex==start_index:
            normal_start1=(np.cross(a1,a2))                                
            
            unit_normal_start= abs(normal_start1/norm(normal_start1))                
            #print("start:",pindex,unit_normal_start)                  
        
        if pindex==end_index:
            normal_end1=(np.cross(a1,a2))              
                
            unit_normal_end= abs(normal_end1/norm(normal_end1))                
            
            #print("end:",pindex,unit_normal_end)
            break               
    
            
    unit_normal=(unit_normal_start+unit_normal_end)/2
        
    #print("Unit normal to terrace plane:",unit_normal)
       
    vec0=pbc_vec(self.h_matrix,self.positions[end_index]-self.positions[start_index] )
    h_teraace=abs(dotproduct(unit_normal, vec0))
    
    #print("Step height(?):",h_teraace)
    return unit_normal, h_teraace;
    
def Find_TerracePlane2(self, data:DataCollection, start_vertex, end_vertex, bvec):
    """Compute the average normal vector among faces connected to the start and end vertices
    but also connecting one vertex with no Burgers vector concent. Use this to compute the step height."""
    
    mesh = data.surfaces['surface']
    
    area_vec = np.zeros(3)
    loopv = (start_vertex,end_vertex)
    #loop over all faces connected to the start and end vertices
    for i in range(1):
        edge = mesh.topology.first_vertex_edge(loopv[i])
        while edge != -1:
            edge2 = mesh.topology.next_face_edge(edge)
            edge3 = mesh.topology.prev_face_edge(edge)
        
            vertex1 = mesh.topology.first_edge_vertex(edge)
            vertex2 = mesh.topology.first_edge_vertex(edge2)
            vertex3 = mesh.topology.first_edge_vertex(edge3)
            
            bnorm1 = bvec[edge,3]
            bnorm2 = bvec[edge2,3]
            bnorm3 = bvec[edge3,3]
            
            #only use faces that do not have line defects penetrating them
            if bnorm1<self.btol and bnorm2<self.btol and bnorm3<self.btol:
                posv1 = mesh.vertices['Position'][vertex1]
                v12 = pbc_vec(self.h_matrix,mesh.vertices['Position'][vertex2]-posv1)
                v13 = pbc_vec(self.h_matrix,mesh.vertices['Position'][vertex3]-posv1)
                
                #compute the area vector of this face and add it to the total
                area_vec += np.cross(v12,v13)
            
            edge = mesh.topology.next_vertex_edge(edge)
    #compute the average normal vector among all faces
    if norm(area_vec)==0:
        n = np.zeros(3)
    else:
        n = area_vec/norm(area_vec)
    
    #compute the step height
    if norm(area_vec)==0:
        h = 0
    else:
        h = abs(np.dot(n,pbc_vec(self.h_matrix,mesh.vertices['Position'][end_vertex]-mesh.vertices['Position'][start_vertex])))
    
    return n,h