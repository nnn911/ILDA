##### Interfacial Line Defect Analysis (ILDA) #####
#
# ILDA is a Python-based tool for identifying and extracting interfacial line defects in atomistic datasets.
#

import numpy as np
from ovito.data import DataCollection
from ovito.pipeline import ModifierInterface
from traits.api import Array, Bool, Float, Int, List, Range, Tuple, Union

from ILDA.main import ILDA


class ILDAModifier(ModifierInterface):
    only_selected = Bool(False, label="Use only selected particles")
    atomA = Union(None, Int, label="Atom 'A' (identifier)")
    atomB = Union(None, Int, label="Atom 'B' (identifier)")
    cis_tol = Range(low=0.0, high=None, value=0.0, label="Co-incidence site tolerance")
    extract_lines = Bool(False, label="Extract lines")
    aA = Range(low=0.0, high=None, value=1.0, label="Lattice constant 'A'")
    aB = Range(low=0.0, high=None, value=1.0, label="Lattice constant 'B'")
    Rsphere = Range(low=0.0, high=None, value=10.0, label="Probe sphere radius")
    btol = Range(low=0.0, high=None, value=0.25, label="Burgers vector magnitude tolerance")
    angtol = Range(low=0.0, high=360.0, value=10.0, label="Burgers vector angular tolerance")
    distF = Range(low=0.0, high=None, value=5.0, label="Interface skin distance")
    estimateF = Bool(True, label="Estimate coherent frame")
    xA = Union(
        None,
        Tuple(Int, Int, Int),
        List(Int, minlen=3, maxlen=3),
        label="Orientation x 'A'",
    )
    yA = Union(
        None,
        Tuple(Int, Int, Int),
        List(Int, minlen=3, maxlen=3),
        label="Orientation y 'A'",
    )
    xB = Union(
        None,
        Tuple(Int, Int, Int),
        List(Int, minlen=3, maxlen=3),
        label="Orientation x 'B'",
    )
    yB = Union(
        None,
        Tuple(Int, Int, Int),
        List(Int, minlen=3, maxlen=3),
        label="Orientation y 'B'",
    )
    EcohA = Union(
        None,
        Tuple(
            Tuple(Float, Float, Float),
            Tuple(Float, Float, Float),
            Tuple(Float, Float, Float),
        ),
        List(List(Int, minlen=3, maxlen=3), minlen=3, maxlen=3),
        label="Coherency strain 'A'",
    )
    EcohB = Union(
        None,
        Tuple(
            Tuple(Float, Float, Float),
            Tuple(Float, Float, Float),
            Tuple(Float, Float, Float),
        ),
        List(List(Int, minlen=3, maxlen=3), minlen=3, maxlen=3),
        label="Coherency strain 'B'",
    )

    def modify(self, data: DataCollection, frame: int, **kwargs):
        # Input validation
        if "Particle Identifier" not in data.particles:
            raise ValueError(
                "ILDA requires 'Particle Identifier' to work. "
                "No particle identifiers were found in this data collection. "
                "Please use the 'Compute Property' modifier before this modifier to generate them."
            )
        if ("Interatomic Distance" not in data.particles) or (
            "Orientation" not in data.particles
        ):
            raise ValueError(
                "ILDA requires 'Interatomic Distance' and 'Lattice Orientations' "
                "from 'Polyhedral Template Matching (PTM)' output. "
                "Please insert the 'Polyhedral Template Matching (PTM)' modifier "
                "with the 'Interatomic distances' and the 'Lattice Orientations' options "
                "activated into the pipeline before this modifier."
            )
        if "Grain" not in data.particles:
            raise ValueError(
                "ILDA requires 'Grain Segmentation' output. "
                "Please insert the 'Grain Segmentation' modifier into the pipeline before this modifier."
            )
        if (
            (not self.atomA)
            or (not self.atomB)
            or (self.atomA not in data.particles["Particle Identifier"])
            or (self.atomB not in data.particles["Particle Identifier"])
        ):
            raise ValueError("Atom A and Atom B need to be valid particle identifiers.")
        if not self.estimateF:
            if self.xA is None:
                raise ValueError(
                    "Please provide a tuple of length 3 for 'Orientation x A' when 'Estimate coherent frame' is not set."
                )
            if self.yA is None:
                raise ValueError(
                    "Please provide a tuple of length 3 for 'Orientation y A' when 'Estimate coherent frame' is not set."
                )
            if self.xB is None:
                raise ValueError(
                    "Please provide a tuple of length 3 for 'Orientation x B' when 'Estimate coherent frame' is not set."
                )
            if self.yB is None:
                raise ValueError(
                    "Please provide a tuple of length 3 for 'Orientation y B' when 'Estimate coherent frame' is not set."
                )

        if self.EcohA is None:
            EcohA = ((0, 0, 0), (0, 0, 0), (0, 0, 0))
        else:
            EcohA = self.EcohA
        if self.EcohB is None:
            EcohB = ((0, 0, 0), (0, 0, 0), (0, 0, 0))
        else:
            EcohB = self.EcohB

        # Run ILDA
        algo = ILDA(
            data,
            self.only_selected,
            self.atomA,
            self.atomB,
            self.cis_tol,
            self.extract_lines,
            self.aA,
            self.aB,
            self.Rsphere,
            self.btol,
            self.angtol,
            self.distF,
            self.estimateF,
            np.array(self.xA),
            np.array(self.yA),
            np.array(self.xB),
            np.array(self.yB),
            np.array(EcohA),
            np.array(EcohB),
        )

        yield from algo.run(data)
