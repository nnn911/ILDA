import ovito
from ovito.data import *
from ovito.modifiers import *
from ovito.vis import *

import numpy as np
from numpy.linalg import norm

import ILDA.utilities
from ILDA.utilities import *

import sys

#set the maximum recursion depth, default for Python is 1000
ntryMax=200
if ntryMax>1000:
    sys.setrecursionlimit(ntryMax)

#tolerance for closure failure of spatial sums
tol1=1e-6

def recursiveCircuitSearch(self, atom_index, start_index, end_index, target_spatial_sum, ntry, spatial_sum, lattice_sum, prev_atoms, Ft2, ref_orientation2):
    """Perform a recursive Burgers circuit search which spans from start_index to end_index, keep track of spatial and lattice sums along the way"""        
    #if we are at the end, return
    if atom_index==end_index:
        if norm(spatial_sum-target_spatial_sum)>tol1:
            return -1
        else:
            return [[atom_index,end_index],spatial_sum,lattice_sum]              
            
    #add this atom to the list of previously visited atoms    
    prev_atoms.append(atom_index)
    
    #if we hit the recursion limit, abort
    ntry+= 1
    if ntry > ntryMax:
        print("ntry exceeded maximum value,")
        return None   
    
    # Create a local neighbor query object.
    ptm_query = PTMNeighborFinder.Query(self.neigh_finder)
    ptm_query1 = PTMNeighborFinder.Query(self.neigh_finder)
    
    grainid = self.grain[atom_index]
    
    if grainid==self.g1_id:
        ref_orientation=self.g1_orientation 
        gs=self.g1_s
        Ft=self.FA
        a=self.aA
        grain_ind = 0
    elif grainid==self.g2_id:
        ref_orientation=self.g2_orientation
        gs=self.g2_s
        Ft=self.FB 
        a=self.aB
        grain_ind = 1
        
    PTMscale=scalePTMVec(gs,a)
    
    #if part of a stacking fault, use ref_orientation2
    if gs == PolyhedralTemplateMatchingModifier.Type.FCC and self.structure_types[atom_index] == PolyhedralTemplateMatchingModifier.Type.HCP:
        ptm_query.find_neighbors(atom_index, ref_orientation2)
    else:
        ptm_query.find_neighbors(atom_index, ref_orientation)       
    
    neighs=[]
    # if atom_index==start_index:
    #     neigh_index = self.cisnbrs[atom_index,grain_ind]
    #     neigh_delta = pbc_vec(self.h_matrix,self.positions[neigh_index]-self.positions[atom_index])
    #     a1 = self.cis_ideal_vecs[atom_index,grain_ind,:]*PTMscale
    #     neighs.append((0,neigh_index,neigh_delta,a1,Ft2,ref_orientation2))
    # else:
    #Find the neighbor atom with min distance from the end_index 
    for j in range(ptm_query.count):            
        neigh = ptm_query[j] 

        #check if neighbor of the called atom is the end_index, if it is break because we're done
        if neigh.index == end_index:
            neighs.append((0.0,neigh.index,neigh.delta,np.array(neigh.ideal_vector)*PTMscale,Ft2,ref_orientation2))
            break
        
        #skip if the atom has the wrong structure type, is not part of the selection, or belongs to the wrong grain
        # if self.structure_types[neigh.index] != gs or self.selection[neigh.index]==0 or grainid != self.grain[neigh.index]:
        if self.selection[neigh.index]==0 or grainid!=self.grain[neigh.index]:
            continue                    
        
        #make sure we haven't previously visited this atom
        if prev_atoms.count(neigh.index)>0:
            continue
        
        #if we're on an FCC atom and the neighbor is an HCP atom    
        if gs == PolyhedralTemplateMatchingModifier.Type.FCC and self.structure_types[neigh.index] == PolyhedralTemplateMatchingModifier.Type.HCP\
           and self.structure_types[atom_index] == PolyhedralTemplateMatchingModifier.Type.FCC:
            Ft2new,ref_orientation2new = GetHCPFCCTrans(self,neigh,atom_index,ptm_query,neigh.index)
            if Ft2new is None:
                continue
        #skip if the neighbor atom's phase doesn't match the grain or the current atom
        elif self.structure_types[neigh.index] != gs and self.structure_types[neigh.index] != self.structure_types[atom_index]:
            continue
        #if we're on an HCP atom and the neighbor is an FCC atom
        elif gs == PolyhedralTemplateMatchingModifier.Type.FCC and self.structure_types[neigh.index] == PolyhedralTemplateMatchingModifier.Type.FCC\
             and self.structure_types[atom_index] == PolyhedralTemplateMatchingModifier.Type.HCP:
            Ft2new = np.identity(3)
            ref_orientation2new = np.zeros(4)
        else:
            Ft2new = Ft2
            ref_orientation2new = ref_orientation2
        
        #compute and store the distance from the end_index    
        # vec=pbc_vec(self.h_matrix,self.positions[end_index]-self.positions[atom_index])
        # vec_sf=pbc_vec(self.h_matrix,self.positions[end_index]-self.positions[start_index])
        # vec_mid=pbc_vec(self.h_matrix,self.positions[atom_index]+ptm_query[j].delta-self.positions[start_index])
        # anglej=abs(angle(pbc_vec(self.h_matrix,ptm_query[j].delta),vec_sf))
        # anglej=abs(angle(ptm_query[j].delta,vec_sf))
        # anglej=abs(angle(vec_mid,vec_sf))
        #distj=norm(pbc_vec(self.h_matrix,self.positions[atom_index]+ptm_query[j].delta-self.positions[end_index]))
        distj = norm(spatial_sum+ptm_query[j].delta-target_spatial_sum)
        neighs.append((distj,neigh.index,np.array(neigh.delta),np.array(neigh.ideal_vector)*PTMscale,Ft2new,ref_orientation2new))
        
    #sort the list based on distances
    neighs.sort()
    
    #loop over the list of neighbors until we find a good Burgers circuit
    for _,neigh_index,neigh_delta,a1,Ft2new,ref_orientation2new in neighs: 
        aa=np.matmul(Ft,np.matmul(Ft2,a1))  
        #skip if the neighbor vector has zero length
        if norm(aa)< 0.001:
            return -1
        
        # #if we made it to the end, return
        # if neigh_index == end_index:
        #     #make sure the spatial sum is correct, this ensures the correct PBC images were chosen
        #     if norm(spatial_sum+ptm_query[j].delta-vec_sf)>tol1:
        #         return -1
        #     return [(atom_index, neigh_index, neigh_delta, tuple(aa))]
      
        result = recursiveCircuitSearch(self, neigh_index, start_index, end_index, target_spatial_sum, ntry,spatial_sum + neigh_delta, lattice_sum + aa, prev_atoms, Ft2new, ref_orientation2new)
        
        #if we returned -1, the atom led to a dead end so go try the next neighbor
        if result==-1:
            continue;
        
        #if we successfully completed the circuit, add info for this step in the circuit
        if not result is None:                    
            # result.append((atom_index, neigh_index, pbc_vec(self.h_matrix,neigh_delta), tuple(aa)))
            result[0].insert(0,atom_index)
            return result
        #otherwise, something was wrong so abort
        return None
        
    #if we get all the way through the for loop, then this must be a dead end so return -1
    if atom_index==start_index:
        print("Burgers circuit failure. Start index:",start_index,"End index:",end_index)
        return None
    else:
        return -1

def BurgersCircuit(self, start_index, end_index):
    """Construct a complete Burgers circuit when includes the start_index and end_index atoms"""
                    
    if not self.cis[start_index] or not self.cis[end_index]: 
        print("Wrong start or end indices: not a co-incident site", start_index,end_index)
        return None
    if start_index==end_index: 
        print("Wrong start or end indices, both are same:",start_index)
        return None
    
    #collect grain props together
    PTMscales = [scalePTMVec(self.g1_s,self.aA),scalePTMVec(self.g2_s,self.aB)]
    Fs = [self.FA,self.FB]
    
    #choose the end point image which is nearest to the start point
    start_point = self.positions[start_index]
    end_point = self.positions[start_index] + pbc_vec(self.h_matrix,self.positions[end_index]-self.positions[start_index])
    
    #construct the two half circuits
    lattice_sum = np.zeros(3)
    spatial_sum = np.zeros(3)
    for i in range(2):
        #unpack the neighbor points and their spatial vectors
        start_nbr = self.cisnbrs[start_index,i]
        end_nbr = self.cisnbrs[end_index,i]
        start_delta = self.cis_spatial_vecs[start_index,i,:]
        end_delta = self.cis_spatial_vecs[end_index,i,:]
        
        #the target spatial sum spans between the two neighbor points
        target_spatial_sum = end_point+end_delta-(start_point+start_delta)
        circuit = recursiveCircuitSearch(self, start_nbr, start_nbr, end_nbr, target_spatial_sum, 0, np.zeros(3),np.zeros(3),[],np.identity(3),np.zeros(4))
        
        #if we encounted an issue, abort
        if circuit is None or circuit==-1:
            return None
        
        #unpack the circuit and add to the sums, careful about the sign!
        circuit_atoms,spatial_sumi,lattice_sumi = circuit
        signi = 1-2*i
        lattice_sum += signi*lattice_sumi
        spatial_sum += signi*spatial_sumi
        
        #add the initial and final circuit steps
        start_ideal_vector = np.matmul(Fs[i],self.cis_ideal_vecs[start_index,i,:]*PTMscales[i])
        end_ideal_vector = np.matmul(Fs[i],self.cis_ideal_vecs[end_index,i,:]*PTMscales[i])
        lattice_sum += signi*(start_ideal_vector-end_ideal_vector)
        spatial_sum += signi*(start_delta-end_delta)
        
    #if the spatial sum is closed, return the Burgers vector, otherwise print that there was an issue
    if norm(spatial_sum)< tol1 :
        return lattice_sum, np.zeros(3), 0 
    else:
        print("Warning: Not a closed circuit!", start_index,end_index,lattice_sum,spatial_sum)
        print(circuit)
        #print(circuit_g2)
        return None